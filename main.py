import math


# задаємо функцію
def f(x):
    return math.pow(math.tan(x), 2) - x


print("Введіть ліву межу: ")
a = float(input())
print("Введіть праву межу: ")
b = float(input())
eps = 1e-5


# алгоритм методу дихотомії (половинне ділення)
def MPD(f, a, b):
    global x
    while abs(b - a) > eps:
        x = (a + b) / 2.0
        fx = f(x)
        fa = f(a)
        if (fx < 0 and fa < 0) or (fx > 0 and fa > 0):
            a = x
        else:
            b = x
        print("Ліва межа: ", "{:.5f}".format(a), "Середина відрізку: ", "{:.5f}".format((a + b) / 2.0),
              " |  Права межа: ", "{:.5f}".format(b))
    return x


x = MPD(f, a, b)
print("Корінь рівняяння на відрізку від ", a, " до ", b, "з точністю ", eps, " : ", x)
print("Нев'язка = ", -f(x)+f(0.694899))


