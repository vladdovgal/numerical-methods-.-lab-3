import math
from scipy.optimize import newton
from sklearn.utils.testing import assert_almost_equal

from main import MPD


def f(x):
    return math.pow(math.tan(x), 2) - x


def df(x):
    return 2 * math.sin(x) / math.pow(math.cos(x), 3)


def dx(f, x):
    return abs(0 - f(x))


def newtons_method(f, df, x0, e, print_res=False):
    delta = dx(f, x0)
    while delta > e:
        x0 = x0 - f(x0) / df(x0)
        delta = dx(f, x0)
        print("Проміжне значення x0: ", x0)
    if print_res:
        print("Розв'язок методом Ньютона (січних) : ", x0)
        # print('Значення функції в даній точці: ', f(x0))
    return x0

# Перевірка правильності результату за допомою бібліотечної функції
def test_with_scipy(f, df, x0s, e):
    for x0 in x0s:
        my_newton = newtons_method(f, df, x0, e)
        scipy_newton = newton(f, x0, df, tol=e)
        assert_almost_equal(my_newton, scipy_newton, decimal=5)
        print('Перевірку пройдено .')



if __name__ == '__main__':
    # run test
    x0s = [0.5]  # масив початкових наближень
    # test_with_scipy(f, df, x0s, 1e-5)
    x_polov_dil = MPD(f, x0s[0], 1)
    for x0 in x0s:
        newtons_method(f, df, x0, 1e-10, True)

    print("Розв'язок методом половинного ділення: ", x_polov_dil)




